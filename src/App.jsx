import RegistroUsuario from './components/Registro'
import { Formulario } from './components/formulario'
import { Home } from './components/Home'
import { useState } from 'react'
import './App.css'

function App() {

  const [user, setUser] = useState([])

  return (
    
  <div className="App">
    {
      !user.length > 0
      ? <Formulario setUser={setUser} />
      : <Home user={user} setUser={setUser} />
    }
    <RegistroUsuario />
  </div>
  )
  
}

export default App
