import React, { useState } from 'react';

const RegistroUsuario = () => {
  const [usuario, setUsuario] = useState({
    nombre: '',
    correo: '',
    contraseña: '',
  });

  const [errores, setErrores] = useState({
    nombre: '',
    correo: '',
    contraseña: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUsuario({
      ...usuario,
      [name]: value,
    });
    // Limpiar el mensaje de error al cambiar el valor del campo
    setErrores({
      ...errores,
      [name]: '',
    });
  };

  const validarFormulario = () => {
    let tieneErrores = false;
    const nuevosErrores = {
      nombre: '',
      correo: '',
      contraseña: '',
    };

    // Validar que todos los campos estén llenos
    if (!usuario.nombre.trim()) {
      nuevosErrores.nombre = 'El nombre es obligatorio';
      tieneErrores = true;
    }

    if (!usuario.correo.trim()) {
      nuevosErrores.correo = 'El correo es obligatorio';
      tieneErrores = true;
    }

    if (!usuario.contraseña.trim()) {
      nuevosErrores.contraseña = 'La contraseña es obligatoria';
      tieneErrores = true;
    }

    setErrores(nuevosErrores);
    return !tieneErrores;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  
    // Validar antes de enviar el formulario
    if (validarFormulario()) {
      // Aquí puedes agregar lógica para enviar los datos del usuario al servidor o realizar otras acciones.
      console.log('Datos del usuario:', usuario);
  
      // Reiniciar el estado del usuario después del envío exitoso
      setUsuario({
        nombre: '',
        correo: '',
        contraseña: '',
      });
    } else {
      console.log('El formulario contiene errores. No se puede enviar.');
    }
  };
  

  return (
    <div className="registro-usuario-container">
      <h2>Registro de Usuario</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Nombre:
          <input
            type="text"
            name="nombre"
            value={usuario.nombre}
            onChange={handleChange}
          />
          <div className="mensaje-error">{errores.nombre}</div>
        </label>
        <br />
        <label>
          Correo:
          <input
            type="email"
            name="correo"
            value={usuario.correo}
            onChange={handleChange}
          />
          <div className="mensaje-error">{errores.correo}</div>
        </label>
        <br />
        <label>
          Contraseña:
          <input
            type="password"
            name="contraseña"
            value={usuario.contraseña}
            onChange={handleChange}
          />
          <div className="mensaje-error">{errores.contraseña}</div>
        </label>
        <br />
        <button type="submit">Registrarse</button>
      </form>
    </div>
  );
};

export default RegistroUsuario;
