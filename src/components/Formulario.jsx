import "./formulario.css";
import { useState } from "react";

export function Formulario({ setUser }) {
  const [nombre, setNombre] = useState("");
  const [contraseña, setContraseña] = useState("");
  const [error, setError] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (nombre === "" || contraseña === "") {
      setError(true);
      return;
    }
    setError(false);

    setUser([nombre]);
  };

  return (
    <section>
      <h1>Login</h1>

      <form className="formulario" onSubmit={handleSubmit}>
        <div>
          <label htmlFor="nombre">Nombre de usuario:</label>
          <input
            type="text"
            id="nombre"
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
          />
        </div>

        <div>
          <label htmlFor="contraseña">Contraseña:</label>
          <input
            type="password"
            id="contraseña"
            value={contraseña}
            onChange={(e) => setContraseña(e.target.value)}
          />
        </div>

        <button type="submit">Iniciar Sesión</button>
      </form>
      {error && <p>Todos los campos son obligatorios</p>}
    </section>
  );
}
